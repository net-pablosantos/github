import os
import sys
import dataframe_processing as df

def validation(pFile, pFileToLoad, pFileInMemory, vFileColumns, pFileColumnsType):
    vFileName = pFile
    vFilePathProcessing = pFileToLoad[10]
    vFileRowStart = pFileToLoad[14]
    vDuplicateRow = pFileToLoad[16]
    vFlHeader = pFileToLoad[21]
    vNrSkipFooter = pFileToLoad[22]
    vCompleteColumns = pFileToLoad[23]
    vTpDecimal = pFileToLoad[24]
    vListColumnsType  = []

    # Processa arquivo no Dataframe e executa validacoes
    vListInsertFinal, vListInsertRejected, vQtRowsLoad = df.dataframe_load(pFileToLoad, pFileInMemory, vFileColumns, pFileColumnsType)
   # Preenche dicionario de dados para envio do email
    vDictFileHistory = {'1 - File Name':vFileName}
    vDictFileHistory['2 - Loaded'] = vQtRowsLoad
    vDictFileHistory['3 - Inserted'] = len(vListInsertFinal)
    vDictFileHistory['4 - Rejected'] = len(vListInsertRejected)
    vSizeFile = os.path.getsize(vFilePathProcessing+vFileName)

    vSizeFile = round(float(vSizeFile/1024))
    vSizeFile = "{:,}".format(vSizeFile)
    vDictFileHistory['5 - Header'] = vFlHeader   
    vDictFileHistory['6 - Row Start'] = vFileRowStart
    vDictFileHistory['7 - Row Skip Footer'] = vNrSkipFooter
    vDictFileHistory['8 - Size'] = vSizeFile.replace(',', '.') + ' KB'

    return vListInsertFinal, vListInsertRejected, vDictFileHistory

    #***********

def validation_ml(pFile, pFileToLoad, pFileInMemory):
    vFileName = pFile
    vFilePathProcessing = pFileToLoad[10]
    vFileRowStart = pFileToLoad[14]
    vDuplicateRow = pFileToLoad[16]
    vFlHeader = pFileToLoad[21]
    vNrSkipFooter = pFileToLoad[22]
    vCompleteColumns = pFileToLoad[23]
    vTpDecimal = pFileToLoad[24]
    vListColumnsType  = []

    # Processa arquivo no Dataframe e executa validacoes
    vListInsertFinal, vListInsertRejected, vQtRowsLoad = df.dataframe_load(pFileToLoad, pFileInMemory, None, None)
   # Preenche dicionario de dados para envio do email
    vDictFileHistory = {'1 - File Name':vFileName}
    vDictFileHistory['2 - Loaded'] = vQtRowsLoad
    vDictFileHistory['3 - Inserted'] = len(vListInsertFinal)
    vDictFileHistory['4 - Rejected'] = len(vListInsertRejected)
    vSizeFile = os.path.getsize(vFilePathProcessing+vFileName)

    vSizeFile = round(float(vSizeFile/1024))
    vSizeFile = "{:,}".format(vSizeFile)
    vDictFileHistory['5 - Header'] = vFlHeader   
    vDictFileHistory['6 - Row Start'] = vFileRowStart
    vDictFileHistory['7 - Row Skip Footer'] = vNrSkipFooter
    vDictFileHistory['8 - Size'] = vSizeFile.replace(',', '.') + ' KB'

    return vListInsertFinal, vListInsertRejected, vDictFileHistory