#-*- coding: utf-8 -*-
import sys
import os
import glob
import fnmatch
import re
from datetime import datetime
#-------- .py Netpartners
import settings as se
import log
import commands_execute as ce
import load_file_config as file_config
import database as db
import move_file as mv
import email_smart as email
import process_validation as pv
import get_nr_load_control as nrlc
import get_list_constant as lc
import send_file_to_database as to_database
import update_lighthouse
import load_file_csv as file_csv
import load_file_xlsx as file_xlsx
import load_file_txt_position as file_txt
#import load_file_sftp as sftp

#-------- End .py Netpartners

#sftp.load_file_sftp()
	
#try:         //hersheys.smt johnson.smt
def StartSmart(pNameFile = None, pId_Company = None):
	if pNameFile != None:
		se.init()
		#Open Connections
		con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
		#GET FILES LIST
		vbFilestoLoad = db.execute_select(con, 'SELECT DISTINCT F.*, M.NM_SHEET FROM PUBLIC.TB_FILES F INNER JOIN PUBLIC.TB_FILES_MAP M ON M.ID_INTERFACE = F.ID_INTERFACE WHERE NM_FILE = %s and F.ID_COMPANY = %s  ORDER BY F.NM_FILE, F.ID_INTERFACE', [pNameFile , pId_Company])
		#get Send email list
		vListFiles = vbFilestoLoad.fetchall()

		if len(vListFiles) == 0:
			pListError = ['Arquivo ou interface não parametrizado(a).']
			log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
			sys.exit()
		Realiza_carga(vListFiles)

	else:
		# Load settings config
		se.init()
		#Open Connections
		con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
		#GET FILES LIST
		vbFilestoLoad = db.execute_select(con, 'SELECT DISTINCT F.*, M.NM_SHEET FROM PUBLIC.TB_FILES F INNER JOIN PUBLIC.TB_FILES_MAP M ON M.ID_INTERFACE = F.ID_INTERFACE WHERE NM_LISTENER = %s ORDER BY F.NM_FILE, F.ID_INTERFACE', [se.CNM_LISTENER])
		#get Send email list
		vListFiles = vbFilestoLoad.fetchall()
		if len(vListFiles) == 0:
			pListError = ['A interface não está parametrizada.']
			log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
			sys.exit()
		Realiza_carga(vListFiles)
		
	return vListFiles
	
def Realiza_carga(vListFiles):
	try:
		con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
		con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])
		vId_company = vListFiles[0][0]	
		vListEmail = db.execute_select(con, 'select ds_mail_address from mmd_schema.tb_mmd_contact_users where fl_send_email_bw_process = %s and id_company = %s', ['Y', vId_company]).fetchall()
		if vListEmail == []:
			pListError = ['Nenhum email foi encontrado na tabela: mmd_schema.tb_mmd_contact_users.']
			log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
			vListEmail = [''.join(x) for x in vListEmail]
			
		sheet=1
		for file in vListFiles:
			vId_company = file[0]
			vIdInterface = file[1]
			vNmClient = file[3]
			vNmProject = file[4]
			vNmFile = file[5]
			vNmTable = file[6]
			vNmTableLog = vNmTable + '_log'
			vFileExtension = file[7]
			vFileSeparator = file[8]
			vFilePathLoad = file[9]
			vFilePathProcessing = file[10]
			vEmailResponsable = vListEmail
			vSendEmailSucess = file[17]
			vTpLoadTable = file[18]
			vNmTablespace = file[19]
			vNmEncoding = file[20]
			vFileError = file[25]
			vTpLoadFile = file[28]  # special for .xlsx
			vSheetName = file[30] # column from tb_files_map]
			set_schema = file [29]
    		
			vFile = glob.glob(vFilePathLoad + vNmFile + '*' + vFileExtension)
			for vFile in vFile:
				vFile = ''.join(vFile)
				try:
					vScript = 'SELECT MMD_SCHEMA.TB_MMD_COMPANY.CD_COMPANY, MMD_SCHEMA.TB_MMD_SYSTEM_SOURCES.CD_SYSTEM_SOURCE '\
					'FROM MMD_SCHEMA.TB_MMD_LIGHTHOUSE '\
					'INNER JOIN MMD_SCHEMA.TB_MMD_COMPANY ON ( MMD_SCHEMA.TB_MMD_COMPANY.ID_COMPANY = MMD_SCHEMA.TB_MMD_LIGHTHOUSE.ID_COMPANY )'\
					'INNER JOIN MMD_SCHEMA.TB_MMD_SYSTEM_SOURCES ON ( MMD_SCHEMA.TB_MMD_SYSTEM_SOURCES.ID_SYSTEM_SOURCE = MMD_SCHEMA.TB_MMD_LIGHTHOUSE.ID_SYSTEM_SOURCE )'\
					'WHERE MMD_SCHEMA.TB_MMD_LIGHTHOUSE.ID_INTERFACE = ' + str(vIdInterface)
					vListReturn = db.execute_select(con, vScript).fetchall()
					
				except:
							
					pListError = ['Erro de parametrização. Consulte a tabela TB_MMD_LIGHTHOUSE']
					log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError, vEmailResponsable)

				mv.rename_file(vFilePathLoad, vFilePathLoad + os.path.basename(vFile), vFilePathLoad + os.path.basename(vFile))
				vFile = vFile.lower()
				if os.path.isfile(vFile):
					sheets = db.execute_select(con, 'SELECT count( DISTINCT M.NM_SHEET) FROM PUBLIC.TB_FILES F INNER JOIN PUBLIC.TB_FILES_MAP M ON M.ID_INTERFACE = F.ID_INTERFACE WHERE NM_FILE = %s', [vNmFile]).fetchall()
					if sheets[0][0] > sheet:
						mv.copyfile(vFile, vFilePathProcessing + os.path.basename(vFile))
						sheet = sheet + 1
					else:
						1 == 1 
						#Move Files List to processing
						mv.move_file(vFile, vFilePathProcessing + os.path.basename(vFile))

					# update lighthouse
					update_lighthouse.update_lighthouse('process_start', vIdInterface)
					 
					#get file columns
					vFileColumns = db.execute_select(con, 'SELECT NM_TARGET_COLUMN, TP_TARGET_COLUMN FROM TB_FILES_MAP WHERE ID_INTERFACE = %s ORDER BY ID_COLUMN', [vIdInterface]).fetchall()
					vFileColumnsType = db.execute_select(con, 'SELECT TP_PYTHON_COLUMN FROM TB_FILES_MAP WHERE ID_INTERFACE = %s ORDER BY ID_COLUMN', [vIdInterface]).fetchall()
					vFileColumnsStartEnd = db.execute_select(con, 'SELECT  nr_column_start, nr_column_end FROM tb_files_map WHERE id_interface = %s and nr_column_end notnull  ORDER BY ID_COLUMN', [vIdInterface]).fetchall()

					# Start load to dataframe (in memory)
					print('Start load file: '+ vFile)

					if vFileExtension in ('.txt', '.csv'):
						if len(vFileColumnsStartEnd) > 0:                
							vFileInMemory = file_txt.load_file_txt_position(vFilePathProcessing + os.path.basename(vFile), vFileColumnsStartEnd)
						else:
							vFileInMemory = file_csv.load_file_csv(vFilePathProcessing + os.path.basename(vFile), vFileSeparator, vNmEncoding)
					else:
						if vFileExtension in ('.xlsx'):
							vCellRange = db.execute_select(con, 'SELECT ds_file_position FROM tb_files_map WHERE ID_INTERFACE = %s ORDER BY ID_COLUMN', [vIdInterface]).fetchall()
							vFileInMemory = file_xlsx.load_file_xlsx(vFilePathProcessing + os.path.basename(vFile), vFileColumns, vSheetName, vTpLoadFile, vCellRange)
					if	len(vFileInMemory) != 0:
			            # Get Number Load Control				
						vNrLoadControl = nrlc.get_nr_load_control(con, vId_company, vIdInterface)
			
			            # Execute validation
						df, df_duplicate, vDictFilesHistory = pv.validation(os.path.basename(vFile), file, vFileInMemory, vFileColumns, vFileColumnsType)
						# Get Constant Columns
						df, vFileColumnsCreate = lc.get_list_constant(con, vIdInterface, vNrLoadControl, vFileColumns,df)
						# Validação de erro:
						if len(df_duplicate) > 0:
							if vFileError:
								#Se verdadeiro carga será realizada normalmente:
								to_database.send_database(con_ssba, vNmTable, vFileColumnsCreate, vTpLoadTable, vNmTablespace, df, df_duplicate, vNmTableLog, vDictFilesHistory, vIdInterface, vNmClient, vNmProject, vEmailResponsable,set_schema)
							else:
								to_database.send_status(False, vNmClient, vNmProject, vEmailResponsable, vDictFilesHistory, df_duplicate, vIdInterface)
						else:
							to_database.send_database(con_ssba, vNmTable, vFileColumnsCreate, vTpLoadTable, vNmTablespace, df, df_duplicate, vNmTableLog, vDictFilesHistory, vIdInterface, vNmClient, vNmProject, vEmailResponsable,set_schema)

					else:
						pListError = ['Sheet name: "'+vSheetName+'" not found in file: '+ vFile]
						log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError , vEmailResponsable)					

					#Move Files List to processed
					vSource = os.path.basename(vFile)
					vNrLoadControl = nrlc.get_nr_load_control(con, vId_company, vIdInterface)
					vTarget = os.path.basename(vFile).replace(vFileExtension, '') + '_' + vNrLoadControl + vFileExtension

					mv.rename_file(vFilePathProcessing, vSource, vTarget)
					mv.move_file(vFilePathProcessing+vTarget, se.CPATH_LOCAL_FILES_PROCESSED+vTarget)

				else:
					# files not found
					print('Error in file '+ vFile)
					
		db.close_db(con)
		db.close_db(con_ssba)
	except:			
		1+1
"""
except IOError as e:
	pListError = ["I/O error({0}): {1}".format(e.errno, e.strerror)]
	log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
except ValueError:
	pListError = ["Could not convert data"]
	log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
except:
	exc_type, exc_value, exc_traceback = sys.exc_info()
	pListError = ["Unexpected error:", exc_value]
	log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
	raise
import shutil
fonte = 'C:/Projetos/Smart/files/base_list.xlsx'
destino =  'C:/Projetos/Smart/files/files_to_load'
shutil.copy2 (fonte,destino)
"""