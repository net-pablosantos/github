import os
import codecs

def load_file_txt_position(pFile, pFileColumnsStartEnd):
    
    vLista = []
    vStardEnd = []
    vStardEnd =[(x[0]-1, x[1]-1) for x in pFileColumnsStartEnd]

    arquivo = codecs.open(pFile) 
    for vLinha in arquivo:
        vLista.append([vLinha [slice(*slc)] for slc in vStardEnd])
    arquivo.close()
    print (vLista)
    return vLista
        
