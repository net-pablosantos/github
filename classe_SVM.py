
###############################################################################################
# classe_SVM.py
#
# Autor: Luiz Ricardo Rego
#
# Algoritmo de treino para o de-para de produto
#
################################################################################################
#
# Dado de entrada - Dataset contendo a descrição do produto e a marca
#
################################################################################################

# Instalação dos pacotes settings e database

import pandas as pd
import settings as se
import database as db

#Importar módulo csv
import csv
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from operator import itemgetter

# Importar módulos para o NLTK
import warnings
warnings.filterwarnings("ignore")
from nltk.cluster.util import cosine_distance
from nltk import sent_tokenize, word_tokenize
from gensim.models import Word2Vec
import codecs
from difflib import SequenceMatcher

class SVM(object):

	def __init__(self):
		# Construindo o modelo SVM com Pipeline
		self.modelo = Pipeline([('vect', TfidfVectorizer()), ('clf', SVC(kernel = 'linear', probability = True))])

		# Objeto para Normalização dos labels
		self.encoder = LabelEncoder()

		self.ds = []

	def trainning(self, modelo = None):

		se.init()

		# Abrir a conexão com o Python
		con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
		con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])

		# Grava os resultados em uma lista
		vbFilestoLoad = db.execute_select(con, 'select produto, marca from  public.tb_produto', [se.CNM_LISTENER])
		vListFiles = vbFilestoLoad.fetchall()

		self.ds = vListFiles
		self.ds = set(self.ds)
		self.ds = list(self.ds)

		# Obtendo dados e labels
		data = [Produto for Produto, Marca in self.ds]
		labels = [Marca for Produto, Marca in self.ds]

		# Normalização dos labels
		target = self.encoder.fit_transform(labels)

		# Fit do modelo
		self.modelo.fit(data, target)
		#print(modelo.fit(data, target))

		return self.modelo, self.encoder

	def get_brand(self, modelo, encoder, desc_produto):

		# Probabilidades de um produto
		probs = modelo.predict_proba([desc_produto])
		
		# Probabidades de categorias para o objeto
		guess = [(class_, probs.item(n)) for n, class_ in enumerate(encoder.classes_)]
		
		# Criar uma lista para armazenar os valores com probabilidades superiores a 
		top_marca = []

		# Probabidade ajustada de marcas para um produto determinado na string
		i = 1
		for marca, proba in sorted(guess, key = itemgetter(1), reverse = True):
			if proba > 0.05:
				top_marca.append(marca)
			i+=1
		# Convertendo a lista em um DataFrame
		ds_marca = ''.join(top_marca)
		return ds_marca

	def product_probability(self, product, marca):
		
		lista_prod_marca = []
		lista_prod_marca = [x[0] for x in self.ds if x[1] == marca]
		lista = []
		
		if len(lista_prod_marca) != 0:
			# Fazer um filtro no dataset
			for x in lista_prod_marca:
				ratio = (SequenceMatcher(None, product, x).ratio()/100)*100
				lista.append([product, x, ratio])
		else:
			lista.append([product, '', 0])

		df_produto = pd.DataFrame(lista)
		df_produto.columns = ['Produto Entrada', 'Produto Previsto', 'Probabilidade']
		idx = df_produto.groupby(['Produto Entrada'])['Probabilidade'].transform(max) == df_produto['Probabilidade']
		df_produto = df_produto[idx]

		return df_produto