#-*- coding: utf-8 -*-
import sys
import os
import glob
import pandas as pd
import numpy as np
#-------- .py Netpartners
import settings as se
import load_file_config as file_config
import database as db
import process_validation as pv
import load_file_csv as file_csv
import load_file_xlsx as file_xlsx
import load_file_txt_position as file_txt
import return_Product as rp
import classe_SVM as svm
import settings as se
import email_smart
import load_mongo

#-------- Rego: Módulo para edição do Excel

from xlsxwriter.utility import xl_rowcol_to_cell

#-------- End .py Netpartners

# Load settings config
se.init()
#Open Connections
con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])
	
#GET FILES LIST
vbFilestoLoad = db.execute_select(con, 'SELECT DISTINCT * FROM PUBLIC.TB_FILES  WHERE NM_LISTENER = %s ORDER BY ID_INTERFACE', [se.CNM_LISTENER])
	
#get Send email list
vListFiles = vbFilestoLoad.fetchall()
vId_company = vListFiles[0][0]
# End Load settings config
	
for file in vListFiles:
	vId_company = file[0]
	vIdInterface = file[1]
	vNmFile = file[5]
	vNmTable = file[6]
	vNmTableLog = vNmTable + '_log'
	vFileExtension = file[7]
	vFileSeparator = file[8]
	vFilePathLoad = file[9]
	vFilePathProcessing = file[10]
	vNmEncoding = file[20]
	vTpLoadFile = file[28]  # special for .xlsx
		
	vFile = glob.glob(vFilePathLoad + vNmFile + '*' + vFileExtension)
	for vFile in vFile:
		vFile = ''.join(vFile)		
		vFile = vFile.lower()

		if os.path.isfile(vFile):
			# Start load to dataframe (in memory)
			print('Start load file: '+ vFile)

			if vFileExtension in ('.txt', '.csv'):
					vFileInMemory = file_csv.load_file_csv(vFilePathProcessing + os.path.basename(vFile), vFileSeparator, vNmEncoding)
			else:
				if vFileExtension in ('.xlsx'):
					vFileInMemory = file_xlsx.load_file_xlsx(vFilePathProcessing + os.path.basename(vFile), vTpLoadFile)

			df = pd.DataFrame(vFileInMemory['produtos'])
			## Treinamento do modelo
			# 2017.02.14 - Remover o treinamento nessa fase. O treinamento estará embutido na chamada de return_Product.
			#SVM = svm.SVM()
			#modelo, encoder = SVM.trainning()
			df = rp.return_Product(df)
			number_rows = len(df.index)

			writer = pd.ExcelWriter(se.CPATH_LOCAL_FILES_SUGEST + 'file' +'_sugest.xlsx', engine='xlsxwriter')
			df.to_excel(writer,'Sheet1', index = False)
			workbook = writer.book
			worksheet = writer.sheets['Sheet1']
			percent_fmt = workbook.add_format({'num_format': '0.00%'})
			worksheet.set_column('A:B', 50, None)
			worksheet.set_column('C:C', 14, percent_fmt)
			writer.save()
			writer.close()

			fileToSend = se.CPATH_LOCAL_FILES_SUGEST + 'file' +'_sugest.xlsx'

			language = 'pt-br' # use default value repalce 'None'
			intetion = 'Grettings'

			answer = load_mongo.answer_mongo(language,intetion)

			text = answer
			emailto = 'pablo.santos@netpartners.com.br'
			vDictEmail = {}
			vDictEmail.update(se.vDictEmailLogin)

			email_smart.send_mail_attachement(vDictEmail,emailto,text,fileToSend)