from datetime import datetime
import database as db
import commands_execute as ce
import log
import email_smart as email
import settings as se
import update_lighthouse
import sys

def send_database(con_ssba, vNmTable, vFileColumnsCreate,vTpLoadTable, vNmTablespace, df, df_duplicate, vNmTableLog, vDictFilesHistory, vIdInterface, vNmClient, vNmProject, vEmailResponsable, pSet_schema):
    vListOk = []
    # Start send to database
    if db.table_exists(con_ssba, vNmTable):
        if 'DROP' in vTpLoadTable:
            vListOk.append(db.execute_script(con_ssba, 'DROP TABLE '+ vNmTable , pSet_schema))
            vListOk.append(db.create_table(con_ssba, vNmTable, vFileColumnsCreate, vNmTablespace, pSet_schema))
            vScriptGrant = ce.load_table_grant(se.CPATH_LOCAL_FILES + 'commands/', 'table_grant.txt', pSet_schema , vNmTable)
            vListOk.append(db.execute_script(con_ssba, vScriptGrant, pSet_schema))
        if 'TRUNCATE' in vTpLoadTable:
             vListOk.append(db.execute_script(con_ssba, 'TRUNCATE '+ vNmTable, pSet_schema))
    else:
        vListOk.append(db.create_table(con_ssba, vNmTable, vFileColumnsCreate, vNmTablespace, pSet_schema))
        vScriptGrant = ce.load_table_grant(se.CPATH_LOCAL_FILES + 'commands/', 'table_grant.txt', pSet_schema, vNmTable)
        vListOk.append(db.execute_script(con_ssba, vScriptGrant, pSet_schema))
    vListOk.append(db.execute_script(con_ssba, "set datestyle to SQL,DMY"))
    vListOk.append(db.sql_insert(con_ssba, 'postgre', vNmTable, df, pSet_schema))

    now = datetime.now()
                    
    #Log
    if len(df_duplicate) > 0:
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', df_duplicate, vEmailResponsable)
        vFileColumnsLog = ['TXT_LOG']
        if db.table_exists(con_ssba, vNmTableLog):
            vListOk.append(db.execute_script(con_ssba, 'DROP TABLE '+ vNmTableLog, pSet_schema))
        vListOk.append(db.create_table(con_ssba, vNmTableLog, vFileColumnsLog, vNmTablespace, pSet_schema))
        vListLog = [[line] for line in df_duplicate]
        vListOk.append(db.sql_insert(con_ssba, 'postgre', vNmTableLog, vListLog, pSet_schema))
    if len(vListOk) == vListOk.count(True):
        send_status(True, vNmClient, vNmProject, vEmailResponsable, vDictFilesHistory, df_duplicate, vIdInterface)
    else:
        send_status(False, vNmClient, vNmProject, vEmailResponsable, vDictFilesHistory, df_duplicate, vIdInterface)


def send_status(pSendEmailOk, vNmClient, vNmProject, vEmailResponsable, vDictFilesHistory, df_duplicate, vIdInterface):
    if pSendEmailOk:
        # Start send e-mail information
        vDictEmail = {'Header':vNmClient +' - ' + vNmProject, 'Subject': 'Status da Carga', 'To':vEmailResponsable}
        vDictEmail.update(se.vDictEmailLogin)
        vDictEmail.update(vDictFilesHistory)
        email.send_mail(vDictEmail, df_duplicate, se.CPATH_IMAGES)
        update_lighthouse.update_lighthouse('process_sucess', vIdInterface, vDictFilesHistory)
        # End send e-mail information
    else:
        # Start send e-mail information
        vDictEmail = {'Header':vNmClient +' - ' + vNmProject, 'Subject': 'Status da Carga', 'To':vEmailResponsable}
        vDictEmail.update(se.vDictEmailLogin)
        del vDictFilesHistory ['3 - Inserted']
        del vDictFilesHistory ['4 - Rejected']
        del vDictFilesHistory ['5 - Header']
        del vDictFilesHistory ['6 - Row Start']
        del vDictFilesHistory ['7 - Row Skip Footer']
        del vDictFilesHistory ['8 - Size']
        vDictEmail.update(vDictFilesHistory)
        vDictEmail['Status da carga'] = 'Carga nao realizada'
        email.send_mail(vDictEmail, df_duplicate, se.CPATH_IMAGES)
        # End send e-mail information
        update_lighthouse.update_lighthouse('process_failure', vIdInterface, vDictFilesHistory)