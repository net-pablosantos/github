import os, sys

def load_table_grant(pPath, pFileToLoad, pNmSchema, pNmTable):
    vScript = ''
    f = open(pPath + pFileToLoad, "r")
    for line in f.readlines():
        vScript = vScript + line
    f.close
    vScript = vScript.replace('<schema_name>', pNmSchema).replace('<table_name>', pNmTable)
    return vScript
