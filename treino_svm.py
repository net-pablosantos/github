
###############################################################################################
# treino_smv.py
#
# Autor: Luiz Ricardo Rego
#
# Algoritmo de treino para o de-para de produto
#
################################################################################################
#
# Dado de entrada - Dataset contendo a descrição do produto e a marca
#
################################################################################################

# Instalação dos pacotes settings e database

import pandas as pd
import settings as se
import database as db
#Importar módulo csv
import csv
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer


class SVM(object):
	def __init__(self):
		# Construindo o modelo SVM com Pipeline
		self.modelo = Pipeline([('vect', TfidfVectorizer()), ('clf', SVC(kernel = 'linear', probability = True))])

		# Objeto para Normalização dos labels
		self.encoder = LabelEncoder()

	def trainning(self):

		se.init()

		# Abrir a conexão com o Python
		con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
		con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])

		# Grava os resultados em uma lista
		vbFilestoLoad = db.execute_select(con, 'select produto, marca from  public.tb_produto', [se.CNM_LISTENER])
		vListFiles = vbFilestoLoad.fetchall()

		ds = []

		ds = vListFiles
		ds = set(ds)
		ds = list(ds)

		# Obtendo dados e labels
		data = [Produto for Produto, Marca in ds]
		labels = [Marca for Produto, Marca in ds]

		# Normalização dos labels
		target = self.encoder.fit_transform(labels)

		# Fit do modelo
		self.modelo.fit(data, target)
		#print(modelo.fit(data, target))

		return self.modelo, self.encoder