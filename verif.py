from difflib import SequenceMatcher
import numpy as np
import pandas as pd
import settings as se
import database as db
import numpy as np
import pandas as pd
import sys
import xlwt	
import numpy as np
import os , openpyxl


def similar(a, b):
	return SequenceMatcher(None, a, b).ratio()


def apply_cleaning(df):
	cols = df.select_dtypes(include=[np.object]).columns
	# tira acentuação
	df['CLEAR'] = df[cols].apply(lambda x: x.str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8'))
	# tira espaço e coloca tudo em letra maiúsculas
	df['CLEAR'] = df['CLEAR'].str.upper().str.replace(' ', '')
	return df

def compare_string(pValue, pListTable):

	vListValue = apply_cleaning(pd.DataFrame([pValue])).values.tolist()
	vListTable = apply_cleaning(pd.DataFrame(pListTable)).values.tolist()
	vListReturn = []
	for x in vListTable:
		vSimilar = similar(vListValue[0][1], x[1])
		vListReturn.append([vListValue[0][0], vSimilar, x[0]])
		if vSimilar == 1:
			break

	df = pd.DataFrame(vListReturn)
	df.columns = ['Valor', 'Percent', 'Source']

	#df = df.groupby(['Valor', 'Source']).agg({'Percent':'max'})
	idx = df.groupby(['Valor'])['Percent'].transform(max) == df['Percent']
	df = df[idx]
	#df['Percent'] = round(df['Percent'], 4)

	return df.values.tolist()[0]

def verifica_igualdade(pDf, pIdInterface,pNmFile, pSet_schema):

	vListCadastro = []

	#Open Connections
	con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])

	vValidar = db.execute_select(con, 'SELECT * FROM public.tb_relacionamento WHERE id_interface_de = %s', [pIdInterface]).fetchall()

	for i in vValidar:
		vTable_De = i[0]
		vTable_Pa = i[2]
		vDe = i[1]
		vPa = i[3]
		vTbPara = db.execute_select(con, 'SELECT nm_target_table FROM public.tb_files WHERE id_interface = %s', [vTable_Pa]).fetchall()[0][0]
		vString = 'SELECT * FROM  rds_schema.'+ vTbPara
		vPara = db.execute_select(con, vString).fetchall()

	for x in vPara:
		vListCadastro.append(x[vPa])

	vListLine = []

	for vLineTupla in pDf:

		vListtext = []
		vListCompare = compare_string(vLineTupla[vDe], vListCadastro)

		for campo in vLineTupla:

			vListtext.append(campo)

		vListtext.append(vListCompare[1])
		vListtext.append(vListCompare[2])
		vListLine.append(vListtext)

	if vListCompare[1] != 1: 

		#nomeia colunas e cria um data frame realizando a criação do arquvio excel
		columns = []
		vListLine_df = pd.DataFrame(vListLine)
		writer = pd.ExcelWriter('C:/Projetos/Smart/files/Sugest_erros/'+ pNmFile +'_Sugest_erros.xlsx')
		vListLine_df.to_excel(writer,'Sheet1')
		writer.save()
		writer.close()
		
	else:

		return (pDf)