# utilizando a biblioteca do Numpy
import codecs

def load_file_csv(pFile, pFileSeparator, pNmEncoding):
    vListInsert = []

    if 'tab' in pFileSeparator:
        pFileSeparator = '\t'

    #load file
    data = codecs.open(pFile, encoding = pNmEncoding)
    for line in data:
        line = line.replace('\r\n', '')
        
        vListInsert.append(line.split(pFileSeparator))
    data.close()

    return vListInsert