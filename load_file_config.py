import os
import json
#from configparser import ConfigParser

def load_config():
    # LOAD CONFIG.JSON
    vPathFileConfig = os.path.dirname(os.path.abspath(__file__))
    vFile = vPathFileConfig + '/config.json'
    with open(vFile) as json_data:
        vDictConfig = json.load(json_data)
	# Load the configuration file
	
    return vDictConfig