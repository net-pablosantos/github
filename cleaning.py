import numpy as np
import pandas as pd


def apply_cleaning(df, pType):
	cols = df.select_dtypes(include=[np.object]).columns
	# tira acentuação
	df['CLEAR'] = df[cols].apply(lambda x: x.str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8'))
	# tira espaço e coloca tudo em letra maiúsculas
	df['CLEAR'] = df['CLEAR'].str.upper()#.str.replace(' ', '')

	if pType == 'list':
		df = df["CLEAR"].tolist()
	return df