#-*- coding: utf-8 -*-
import pyodbc, psycopg2, psycopg2, os, sys
import settings as se
import log

def db_connection(pDbName, pDbString):
# Description: open Database and return the connection
# Parameters: (pDb = string with conection name)
    if pDbName.lower() == 'postgre':
        try:
            con = psycopg2.connect(pDbString)
        except psycopg2.OperationalError as e:
            pListError = ['Unable to connect!\n{0}'.format(e)]
            log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
    if pDbName.lower() == 'sql':
        try:
            con = pyodbc.connect(pDbString)
        except pyodbc.Error as e:
            pListError = ['Unable to connect!\n{0}'.format(e)]
            log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError) 
    return con
    
def execute_script(pCon, pScript, pSchema = None):
# Description: execute a script with a param connection
# Parameters: (pCon = Connection); (pScript: Script to execution)
    try:
        cur = pCon.cursor() # Criando um cursor
        if pSchema is not None and pSchema != '':
            cur.execute("SET LOCAL search_path TO {}". format(pSchema))
        cur.execute(pScript)
        pCon.commit()
        vOk = True
    except psycopg2.Error as e:
        pListError = ['Unable to execute script!\n{0}'.format(e)]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
        vOk = False
    finally:
        cur.close()
    
    return vOk

def execute_select(pCon, pScript, pParam = None):
# Description: execute a select with param (opcional) and return the cursor
# Parameters: (pCon = Connection); (pScript: Script to execution), (pParam: paramaters to enclosure
    cur = pCon.cursor() # Criando um cursor
    if pParam == None:
        cur.execute(pScript)
    else:
        cur.execute(pScript, pParam)
    return cur


def complete_parameters(pString, pLen):
    pListParamaters = [pString]
    while len(pListParamaters) < pLen:
        pListParamaters.append(pString)
    pListParamaters[len(pListParamaters)-1] = pListParamaters[len(pListParamaters)-1][:-1]
    return ''.join(pListParamaters)

def sql_insert(pCon, pDbName, pTabela, pListValues, pSchema=None):
    vListErrors = []
    try:
        if pSchema is not None and pSchema != '':
            cur = pCon.cursor()
            cur.execute("SET search_path TO {}".format(pSchema))
            pCon.commit()
    except psycopg2.Error as e:
        pListError = ['Unable to insert in table!\n{0}'.format(e)]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)

    if pDbName.lower() == 'postgre':
        vDataAtual = 'CURRENT_TIMESTAMP'
    try:
        vParameters = complete_parameters('%s,', len(pListValues[0]))
        vSql_Insert = 'INSERT INTO '+ pTabela + ' values('+vParameters+')'
    except psycopg2.Error as e:
        pListError = ['Unable to insert in table!\n{0}'.format(e)]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)

    try:
        cur.executemany(vSql_Insert, pListValues)
        pCon.commit()
        vOk = True
    except psycopg2.Error as e:
        pListError = ['Unable to execute insert!\n{0}'.format(e)]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
        pCon.rollback()
        vOk = False
    finally:
        cur.close()

    return vOk

def table_exists(pCon, pNmTable):
    exists = False
    try:
        cur = pCon.cursor()
        cur.execute('SELECT COUNT(1) from information_schema.tables where table_name = %s', [pNmTable])
        exists = cur.fetchone()[0]
        if exists == 1:
            exists = True
        cur.close()
    except psycopg2.Error as e:
        pListError = ['Unable to check table!\n{0}'.format(e)]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
    return exists

def create_table(pCon, pNmTable, pFileColumns, pNmTablespace, pSchema=None):
    vFileColumns = []
    vFileColumns = [''.join(list(x)) for x in pFileColumns]
    cur = pCon.cursor()
    if '_log' in pNmTable:
        vFileColumns = [x + ' varchar(8000),' for x in vFileColumns]
    else:
        vFileColumns = [x + ',' for x in vFileColumns]
    vFileColumns[len(vFileColumns)-1] = vFileColumns[len(vFileColumns)-1][:-1]
    vCreateString = 'CREATE TABLE '+ pNmTable +' (' + ''.join(vFileColumns) + ')  TABLESPACE '+ pNmTablespace
    try:
        if pSchema is not None and pSchema != '':
            cur.execute("SET search_path TO {}".format(pSchema))
        cur.execute(vCreateString)
        pCon.commit()
        vOk = True
    except psycopg2.Error as e:
        pListError = ['Unable to create table!\n{0}'.format(e)]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
        vOk = False
    finally:
        cur.close()
    return vOk

def close_db(pCon):
#Description: close the connection
# Parameters: pCon = Connection
    pCon.close()