################################################################################################

# Carregar o treino SVM

import pandas as pd
import treino_svm as svm
from pprint import pprint
# Carregar o pacote do SkLearn

from sklearn.pipeline import Pipeline
from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from operator import itemgetter


def ProbabilidadeMarca(modelo, encoder, desc_produto):    
	# Probabilidades de um produto
	probs = modelo.predict_proba([desc_produto])
	
	# Probabidades de categorias para o objeto
	guess = [(class_, probs.item(n)) for n, class_ in enumerate(encoder.classes_)]
	
	# Criar uma lista para armazenar os valores com probabilidades superiores a 
	top_marca = []

	# Probabidade ajustada de marcas para um produto determinado na string
	for marca, proba in sorted(guess, key = itemgetter(1), reverse = True):
		if proba > 0.10:
			top_marca.append(marca)
	
	# Convertendo a lista em um DataFrame
	df_marca = pd.DataFrame({'Marcas' : top_marca})
	return df_marca

SVM = svm.SVM()
modelo, encoder = SVM.trainning()
lista = ['hellmanns pic pet 24x200g', 'hellmanns pic most pet 24x200g', 'hellmanns pic most p', 'dove', 'mai']
for x in lista:
	print(ProbabilidadeMarca(modelo, encoder, x))
