#-*- coding: utf-8 -*-
import os
import smtplib
import mimetypes
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.text import MIMEText
import settings as se
import log

#********************* DEFs *********************
def create_html(pEmail, pListDuplicate, pPathImages):
    vText = ''
    #cabeçalho        
    vHtml = '<html>'\
            '<head>'\
            '<table>'\
            '<tr>'\
            '<td><p align="left"> <IMG width="150px" src= "'+pPathImages + 'logo_cliente.png'+'"></p></td>'\
            '<td><p align="right"> <IMG src= "'+pPathImages + 'logo_netpartners.png'+'"></p></td>'\
            '</tr>'\
            '</head>'\
            '<body>'\
            '<div style="margin-left:200px"> <h1>' + pEmail['Header'] + '</h1></div>'\

    for key in sorted(pEmail):
        if key != 'Header' and key != 'Subject' and key != 'To' and key != 'Login' and key != 'Password':
            str1 = str(key)
            str2 = str(pEmail[key])
            texto1 = ''.join(str1)
            texto2 = ''.join(str2)
            texto = '<div style="margin-left:50px"><b>'+ texto1 + ': </b>'+ texto2 +' </div>'
            vHtml = vHtml + '\n' + texto
    vHtml = vHtml + '\n' + '<p>	</p>'
    vHtml = vHtml + '\n' + vText
    
    if len(pListDuplicate) > 0:
        vHtml = vHtml + '<div style="padding:10px;border:2px solid blue;">'
        vHtml = vHtml + '\n' + '<p>	</p>'
        vHtml = vHtml + '\n' + vText
        vText = '<div><b> Segue Abaixo Erros Encontrados: </b> <p></p></div>'
        vHtml = vHtml + '\n' + vText
        for line in pListDuplicate:
            vText = ''.join(line)
            vText = '<div>'+ vText + ' </div>'
            vHtml = vHtml + '\n' + vText
        vHtml = vHtml + '</div>'

    #rodapé
    vHtml = vHtml + '\n' + '<p>	</p>'
    vHtml = vHtml + '\n' + '<p>	</p>'
    vHtml = vHtml + '\n' + '<p>	</p>'
    vHtml = vHtml + '<div style="height:8px;padding:10px;background:rgb(233,228,228);color:black">Por favor, nao responda a este e-mail. Esta e uma mensagem automatica.</div>'
    vHtml = vHtml + '\n' + '</body>', '</html>'
    vbHtml = MIMEText(''.join(vHtml))
    return vHtml

   
def send_mail(pEmail, pListDuplicate, pPathImages):
    #try:
    vHtml = MIMEText('')

    #cria um cliente smtp que conectará em smtp.gmail.com na porta 587
    smtp = smtplib.SMTP("smtp.gmail.com", 587)
    #nos identificamos no servidor
    smtp.ehlo()
    #indicamos que usaremos uma conexão segura
    smtp.starttls()
    #reidentificamos no servidor (necessário apos starttls )
    smtp.ehlo()
    #faz o login utilizando dicionário e parametros
    smtp.login(pEmail['Login'], pEmail['Password'])

    vHtml = create_html(pEmail, pListDuplicate, pPathImages)
    vText = ''.join(vHtml)
    vHeaders = "\r\n".join(["from: " + pEmail['Login'],
                            "subject: " + pEmail['Subject'],
                            "to: " + ''.join(pEmail['To']),
                            "mime-version: 1.0",
                            "content-type: text/html"])

    # body_of_email can be plaintext or html!                    
    vContent = vHeaders + "\r\n\r\n" + vText
    smtp.sendmail(pEmail['Login'], pEmail['To'], vContent)
    #fecha a conexão
    smtp.close()
    #except:
    #    print("Email nao pode ser enviado")
        #pListError = ["Email nao pode ser enviado"]
        #log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)

#********************* FIM DEFs *********************

#Criado na data 16/02/2017 Pablo Santos **************

def send_mail_attachement(pEmail,emailto,text,fileToSend):
	try:
		msg = MIMEMultipart()
		msg["From"] = pEmail['Login']
		msg["To"] = emailto
		msg["Subject"] = ""
		msg.preamble = ""
		msg.attach(MIMEText("<html><head>Resposta via MongoDb</head><body><h1> "  + text + "</h1></body></html>", "html", _charset="utf-8"))

		ctype, encoding = mimetypes.guess_type(fileToSend)
		if ctype is None or encoding is not None:
			ctype = "application/octet-stream"

		maintype, subtype = ctype.split("/", 1)

		if maintype == "text":
			fp = open(fileToSend)
			# Note: we should handle calculating the charset
			attachment = MIMEText(fp.read(), _subtype=subtype)
			fp.close()
		elif maintype == "image":
			fp = open(fileToSend, "rb")
			attachment = MIMEImage(fp.read(), _subtype=subtype)
			fp.close()
		elif maintype == "audio":
			fp = open(fileToSend, "rb")
			attachment = MIMEAudio(fp.read(), _subtype=subtype)
			fp.close()
		else:
			fp = open(fileToSend, "rb")
			attachment = MIMEBase(maintype, subtype)
			attachment.set_payload(fp.read())
			fp.close()
			encoders.encode_base64(attachment)
		attachment.add_header("Content-Disposition", "attachment", filename=fileToSend)
		msg.attach(attachment)

		server = smtplib.SMTP("smtp.gmail.com:587")
		server.starttls()
		server.login(pEmail['Login'], pEmail['Password'])
		server.sendmail(pEmail['Login'], emailto, msg.as_string())
		server.quit()
	except:
		#print("Email nao pode ser enviado")
		pListError = ["Email nao pode ser enviado"]
		log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)


def send_mail_log(pEmail, pListDuplicate, pPathImages):
    try:
        vHtml = MIMEText('')
        #cria um cliente smtp que conectará em smtp.gmail.com na porta 587
        smtp = smtplib.SMTP("smtp.gmail.com", 587)
        #nos identificamos no servidor
        smtp.ehlo()
        #indicamos que usaremos uma conexão segura
        smtp.starttls()
        #reidentificamos no servidor (necessário apos starttls )
        smtp.ehlo()
        #faz o login utilizando dicionário e parametros
        smtp.login(pEmail['Login'], pEmail['Password'])

        vHtml = create_html(pEmail, pListDuplicate, pPathImages)
        vText = ''.join(vHtml)
        vHeaders = "\r\n".join(["from: " + pEmail['Login'],
                               "subject: " + pEmail['Subject'],
                               "to: " + ''.join(pEmail['To']),
                               "mime-version: 1.0",
                               "content-type: text/html"])

        # body_of_email can be plaintext or html!                    
        vContent = vHeaders + "\r\n\r\n" + vText
        smtp.sendmail(pEmail['Login'], pEmail['To'], vContent)
        #fecha a conexão
        smtp.close()

    except:
        #print("Email nao pode ser enviado")
        pListError = ["Email nao pode ser enviado"]
        log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)

