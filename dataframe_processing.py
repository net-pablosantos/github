# utilizando a biblioteca do Numpy
import numpy as np
import pandas as pd
import sys


vListRejected = []
vQtRowsLoad = 0

def dataframe_load(pFileToLoad, df, pFileColumns, pFileColumnsType):

    global vQtRowsLoad, vListRejected
    vFileRowStart = pFileToLoad[14]
    vDuplicateRow = pFileToLoad[15]
    vFlHeader = pFileToLoad[21]
    vNrSkipFooter = pFileToLoad[22]
    vCompleteColumns = pFileToLoad[23]
    vTpDecimal = pFileToLoad[24]
    vRemoveAccentuation = pFileToLoad[26]
    vRemoveLowerLetters = pFileToLoad[27]    
    vListColumnsType  = []
    vQtRowsLoad = len(df)

    if type(df) == list:
        vLenListColumns = len(pFileColumns)
        for x in pFileColumnsType:
            if 'String' in x[0]: vListColumnsType.append(str)
            if 'Number' in x[0]: vListColumnsType.append(float)

        #check qtd columns
        for i, elem in enumerate(df):
            if (vCompleteColumns == True):
                vElemTemp = list_complete(elem, vLenListColumns)
            else:
                vElemTemp = [x for x in elem]
            if len(vElemTemp) != vLenListColumns:
                vListRejected.append('Erro na linha: ' + str(i+1) + ' (Quantidade de Colunas Invalidas) ')
                del df[i]
        df = pd.DataFrame(df)     

    
    #if type(df) == list:
        #df = df.apply(lambda x: x.str.strip()).replace('', np.nan) # substitui colunas contendo apenas espacos para null

    # skip fotter line
    if (vNrSkipFooter > 0): df = del_row_footer(df, vNrSkipFooter)

    # skip header and set lines to start load
    if (vFlHeader == True): df = del_row_header(df, 2)

    if (vFileRowStart > 1): df = del_row_header(df, vFileRowStart)

    # check decimal separator
    if vTpDecimal == ',': df = check_tp_decimal(df, vListColumnsType)

    # check remove accentuation
    if vRemoveAccentuation == True: df = remove_accentuation(df)

    # check remove lower letters
    if vRemoveLowerLetters == True: df = remove_lower_letters(df, vListColumnsType)
    
	# check datatypes
    df = check_datatype(df, vListColumnsType)

    df = df.where(pd.notnull(df), None)
    
	# check remove duplicate lines
    if vDuplicateRow == True:
        vListInsertFinal = check_duplicate_rows(df)
    else:
        vListInsertFinal  = df.values.tolist()

    #clear memory
    del df

    return vListInsertFinal, vListRejected, vQtRowsLoad

# ****************** Validation Defs ******************

def del_row_header(df, pFileRowStart):
    global vQtRowsLoad
    i = 1
    while (i < pFileRowStart):
        df = df.drop(df.index[[0]])
        i = i + 1
        vQtRowsLoad = vQtRowsLoad - 1
    return df

def del_row_footer(df, vNrSkipFooter):
    global vQtRowsLoad

    df = df[:-vNrSkipFooter]
    vQtRowsLoad = vQtRowsLoad - vNrSkipFooter
    return df

def check_datatype(df, pListColumnsType):    
    global vListRejected
    indexes = [i for i,x in enumerate(pListColumnsType) if x == float]
    vListInsertFinal  = df.values.tolist()
    del df
    vListInsertTemp = []
    for i, elem in enumerate(vListInsertFinal): 
        vElemTemp = [x for x in elem ]
        try:
            #out = [fn(v) for fn , v in zip(pListColumnsType, vElemTemp)]
            out=[]
            for fn, v in zip(pListColumnsType, vElemTemp):
                if v == '' and fn == float:
                    out.append(v)
                else:
                    out.append(fn(v))      
            vListInsertTemp.append(vElemTemp)
        except:
            vListRejected.append('Erro na linha: ' + str(i+1) + ' (Tipos de dados invalidos) ')

    df = pd.DataFrame(vListInsertTemp)
    #df = df.replace(np.nan,'', regex=True)
    df = df.replace('', np.nan) # substitui colunas contendo apenas espacos para null.

    return df

def check_duplicate_rows(df):
    global vListRejected
    vListInsertFinal = []
    vFileColumns = df.columns.values.tolist()
    
    #del vFileColumns[len(vFileColumns):]

    df2 = df[df.columns.values.tolist()].groupby(df.columns.values.tolist())
    gb_groups = df2.groups
    for k, v in gb_groups.items():
        vListInsertFinal.append(k)
    # lista todos os registros que possui linha duplicada
    df2 = df[vFileColumns].groupby(vFileColumns)
    gb_groups = df2.groups		
    for k, v in gb_groups.items():
        if len(v) != 1:
            vListRejected.append('Linhas duplicadas: ' + str(v) + ' Dados: ' + str(k))
    return vListInsertFinal

def check_tp_decimal(df, pFileColumnsType):
    indexes = [i for i, x in enumerate(pFileColumnsType) if x == float]
    for i in indexes:
        df[i] = df[i].str.replace('.','')
        df[i] = df[i].str.replace(',', '.')
    return df

def remove_accentuation(df):
    df = df.replace(np.nan,'')
    cols = df.select_dtypes(include=[np.object]).columns
    df[cols] = df[cols].apply(lambda x: x.str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8'))
    return df

def remove_lower_letters(df, pFileColumnsType):
    indexes = [i for i, x in enumerate(pFileColumnsType) if x == str]
    for i in indexes:
        df[i] = df[i].str.upper()
    return df
	
def list_complete(pListValues, pLen):
    while len(pListValues) < pLen:
        pListValues.append(np.nan)
    return pListValues