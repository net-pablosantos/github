import os
import log
import psycopg2
import paramiko
import sys
import email_smart as email
import glob
import database as db
import settings as se
import shutil	
import time
from datetime import date

#def load_file_sftp():
if 1 == 1:
	vToday = date.today()
	vToday = str(vToday.year)+'%02d' % vToday.month+str(vToday.day)

	try:#DATABASE
		# Load settings config
		se.init()
		#Open Connections database
		con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
		con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])

		vConectionFolder = db.execute_select(con,'SELECT DISTINCT sftp_hostname, sftp_user, sftp_pass, dir_file, f.ds_path_load FROM public.tb_files_sftp fs INNER JOIN public.tb_files f ON f.id_company = fs.id_company WHERE nm_listener = %s',[se.CNM_LISTENER])

		#get Send email list
		vConection = vConectionFolder.fetchall()
		vId_company = vConection[0][0]

		for vDataconect in vConection:

			hostname = vDataconect[0]
			username = vDataconect[1]
			password = vDataconect[2]
			dirRemotoRaiz = vDataconect[3]
			dirLocalRaiz = vDataconect[4]

			try:#SFTP
				#Open Connections sftp
				ssh = paramiko.SSHClient()
				ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
				ssh.connect(hostname, username = username, password = password)
				sftp = ssh.open_sftp()

				#GET FILES LIST
				vLista = sftp.listdir(dirRemotoRaiz)#Changing: s.decode(encoding)To:s.decode(encoding,'ignore'in pakpage)
				print (vLista)
				#Lista de arquivos autorizados para carga
				vbFilestoLoad = db.execute_select(con, 'SELECT NM_FILE||TP_EXTENSION FROM PUBLIC.TB_FILES WHERE NM_LISTENER = %s', [se.CNM_LISTENER])
				vListFiles1 = vbFilestoLoad.fetchall()
				vListFiles = []

				for x in vListFiles1:
					vListFiles.append(x[0])	
				vListJoin = [x for x in vListFiles if x in vLista]

				for vFile in vListJoin: #verifica arquivos presentes na pasta do servidor
					statinfo = sftp.stat(dirRemotoRaiz+vFile)
					size_a = statinfo.st_size
					time.sleep(1)
					size_b = statinfo.st_size

					if size_a == size_b:
						try:
							sftp.get(dirRemotoRaiz+vFile,dirLocalRaiz+vFile) #+'_'+vToday #realiza copia dos arquivos do servidor para a maquina.
							#sftp.remove(dirRemotoRaiz+vFile) #apaga os arquivos do servidor para evitar duplicidade.
						except:
							pListError = ['Erro ao tentar realizar a copia do aquivo '+ vFile + ' no SFTP, sera necessario realizar a transferencia manual!']
							log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
							sftp.remove(dirLocalRaiz+vFile) #apaga os arquivos da pasta destino para evitar carga de arquivos corrompidos.

					vLista = sftp.listdir(dirRemotoRaiz)

				#end for
				sftp.close()
				ssh.close()

			except:#SFTP #Unable to connect to SFTP
				pListError = ['Unable to connect to SFTP!']
				log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)

	except:#DATABASE Unable to connect to data base
		pListError = ['Unable to connect to data base!']
		log.add_log(se.CPATH_LOCAL_FILES + 'log/', 'log.txt', pListError)
