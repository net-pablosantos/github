#-*- coding: utf-8 -*-
#-------- .py Netpartners
import load_file_config as file_config
#-------- End .py Netpartners

def init():
	global CPATH_LOCAL_FILES, CPATH_LOCAL_FILES_PROCESSED, CPATH_IMAGES, CNM_LISTENER, CMAIL_SUPORTE, vbDictDatabase, vDictEmailLogin, CPATH_LOCAL_FILES_SUGEST
	
	# Start load Paramaters
	vDictConfig = file_config.load_config()
	CPATH_LOCAL_FILES = vDictConfig['PATH_LOCAL_FILES']
	CPATH_LOCAL_FILES_PROCESSED = vDictConfig['PATH_LOCAL_FILES_PROCESSED']
	CPATH_LOCAL_FILES_SUGEST = vDictConfig['PATH_LOCAL_FILES_SUGEST']
	CPATH_IMAGES = vDictConfig['PATH_IMAGES']
	CNM_LISTENER = vDictConfig['NM_LISTENER']
	
	#Database
	vbDictDatabase = {}
	vbDictDatabase['ODBC'] = vDictConfig['ODBC_NAME']
	vbDictDatabase['Name'] = vDictConfig['DB_NAME']
	vbDictDatabase['String'] = vDictConfig['DB_STRING']
	vbDictDatabase['Name_ssba'] = vDictConfig['DB_SSBA_NAME']
	vbDictDatabase['String_ssba'] = vDictConfig['DB_SSBA_STRING']

	#E-mail
	vDictEmailLogin = {'Login':vDictConfig['MAIL_LOGIN'],'Password':vDictConfig['MAIL_PASSWORD']}
	CMAIL_SUPORTE = vDictConfig['MAIL_SUPPORT']