import os
import paramiko


remote_path = '/hersheys/inbound/bau_itens.txt'
local_path = 'C:/Projetos/Smart/arquivos/'

server, username, password = ('hersheys.ftp.netpartners.com.br', 'ftp.hersheys_inb', 'ph3c8ap3@!_xed')
ssh = paramiko.SSHClient()
paramiko.util.log_to_file('C:/Projetos/Smart/arquivos/teste.txt')
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#In case the server's key is unknown, we will be adding it automatically to the list of known hosts
ssh.load_host_keys(os.path.expanduser("C:/Projetos/Smart/arquivos/known_hosts/a.txt"))
#Loads the user's local known host file.
ssh.connect(server, username=username, password=password)
#ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('ls /tmp')
#print("output", ssh_stdout.read()) #Reading output of the executed command
#error = ssh_stderr.read()
#Reading the error stream of the executed command
#print("err", error, len(error))

#Transfering files to and from the remote machine
sftp = ssh.open_sftp()
for i in sftp.listdir():
    lstatout=str(sftp.lstat(i)).split()[0]
    if 'd' in lstatout: 
        print(i, 'is a directory')
    if 'd' not in lstatout: 
        print(i, 'is a file')
        if i == 'bau_itens.txt':
            sftp.get(remote_path, local_path, None)
        
    

#sftp.put(local_path, remote_path)
sftp.close()
ssh.close()