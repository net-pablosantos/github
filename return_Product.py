####################################
#
# return_Product.py
#
# Autor: Luiz Ricardo Rego
#
####################################

import classe_SVM as svm
import pandas as pd
import cleaning as cl

def return_Product(df):

	# 2017.02.14 - Inclusão da rotina de treinamento do modelo dentro da def.
	# Futuramente remover a rotina.

	## Treinamento do modelo
	try:
		del SVM
	except:
		SVM = svm.SVM()
		modelo, encoder = SVM.trainning()

	# Aplica procedimento de limpeza ao data frame ***REMOVER***

	df_temp = pd.DataFrame()
	df_saida = pd.DataFrame()
	
	## Aplicação do algoritmo de Machine Learning
	list_temp = cl.apply_cleaning(df, 'list')

	j = 1
	for x in list_temp:
		df_temp = [SVM.product_probability(x, SVM.get_brand(modelo, encoder, x))]
		j+=1
		df_saida = df_saida.append(df_temp)
	df = pd.DataFrame(df_saida)

	return df