import database as db
import sys

def get_nr_load_control (con, pId_company, pIdInterface):
	vScript = 'insert into TB_LOAD_CONTROL values (default, '+ str(pId_company) +',' + str(pIdInterface) + ')'
	db.execute_script(con, vScript)
	vNrLoadControl = db.execute_select(con, 'SELECT CAST(MAX(NR_LOAD_CONTROL) AS VARCHAR) NR_LOAD_CONTROL FROM TB_LOAD_CONTROL WHERE ID_INTERFACE = %s', [pIdInterface]).fetchall()

	return list(vNrLoadControl[0])[0]