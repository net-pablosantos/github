import database as db
import settings as se
import datetime

def update_lighthouse(pTypeUpdate, vIdInterface, vDictFilesHistory = None):
    con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])
    data = str(datetime.datetime.now())
    data = '\''+data+'\''

    #trocar para case
    if pTypeUpdate == 'process_start':
        vScript = 'update mmd_schema.tb_mmd_lighthouse set dt_start_processing = '+ data +', cd_status_processing = 1 where id_interface = ' + str(vIdInterface)
    
    if pTypeUpdate == 'process_sucess' and vDictFilesHistory != None:
        vScript = 'update mmd_schema.tb_mmd_lighthouse set dt_end_processing = '+ data +', cd_status_processing = 2, '\
                'nb_read_lines_quantity = ' + str(vDictFilesHistory['2 - Loaded']) + ', ' \
                'nb_load_lines_quantity = ' + str(vDictFilesHistory['3 - Inserted']) + ', ' \
                'nb_rejected_lines_quantity = ' + str(vDictFilesHistory['4 - Rejected']) + ' '\
                'where id_interface  = ' + str(vIdInterface)

    if pTypeUpdate == 'process_failure' or (pTypeUpdate == 'process_sucess' and vDictFilesHistory == None):
        vScript = 'update mmd_schema.tb_mmd_lighthouse '\
                'set dt_end_processing = '+ data +', cd_status_processing = 2, '\
                'nb_read_lines_quantity =  0, ' \
                'nb_load_lines_quantity = 0, ' \
                'nb_rejected_lines_quantity = 0'\
                'where id_interface  = ' + str(vIdInterface)

    db.execute_script(con_ssba, vScript, 'mmd_schema')