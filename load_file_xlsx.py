# utilizando a biblioteca do Numpy
import numpy as np
import pandas as pd
import log
import xlrd
import sys

def colnum_string(n):
    div=n
    string=""
    temp=0
    while div>0:
        module=(div-1)%26
        string=chr(65+module)+string
        div=int((div-module)/26)
    return string

def load_file_xlsx_cell(pDf, pCellRange):
	print(pDf)
	sys.exit()
	vListInsert = []
	vListInsertFinal = []
	for vCell in pCellRange:
		vRowStart = int(''.join([i for i in vCell[0] if i.isdigit()]))
		vRowEnd = vRowStart
		vStart = ''.join([i for i in vCell[0] if not i.isdigit()])
		vEnd = vStart	
		vListInsert.append(pDf.ix[vRowStart-1:vRowEnd-1,vStart:vEnd].values.tolist())
		#.transpose([])
	
	if len(vListInsert) == 0:
		for x in vListInsert:
			vListInsertFinal.append(x[0][0])

		df = pd.DataFrame(vListInsertFinal).transpose()
	else:
		df = pd.DataFrame() 
	return df

def load_file_xlsx_range(pDf, pCellRange):
	for vCell in pCellRange:
		vCell = ''.join(pCellRange[0]).split(":")
		vRowStart = int(''.join([i for i in vCell[0] if i.isdigit()])) - 1
		vRowEnd = int(''.join([i for i in vCell[1] if i.isdigit()])) - 1
		vStart = ''.join([i for i in vCell[0] if not i.isdigit()])
		vEnd = ''.join([i for i in vCell[1] if not i.isdigit()])
		vListInsert = pDf.ix[vRowStart:vRowEnd,vStart:vEnd].values.tolist()
	df = pd.DataFrame(vListInsert)
	
	return df

# ************************ main ************************
def load_file_xlsx(pFile, pFileColumns, pSheetName=None, pTpLoadFile='', pCellRange=''):
    #load file
	xls = xlrd.open_workbook(pFile,on_demand=True)
	sheets = xls.sheet_names()
	if pSheetName in sheets:
		df = pd.read_excel(pFile, sheetname = pSheetName, header=None)
		if 'CELL' in pTpLoadFile:
			print('cell')
			columns_names =[colnum_string(x) for x in range(1,len(df.columns)+1)]
			df.columns = columns_names
			df = load_file_xlsx_cell(df, pCellRange)
		else:
			if 'RANGE' in pTpLoadFile:
				print('range')
				columns_names =[colnum_string(x) for x in range(1,len(df.columns)+1)]
				df.columns = columns_names
				df = load_file_xlsx_range(df, pCellRange)		
	else:
		df = pd.DataFrame()

	return df

