from datetime import datetime
import os
import os.path
import settings as se
import sys
import email_smart as email

def add_log(pPath, pFile, pListText, pEmail = None):
    if pEmail == None: 
        pEmail = se.CMAIL_SUPORTE
        
    if not os.path.exists(pPath):
        os.makedirs(pPath)
    now = str(datetime.now())
    with open(pPath + pFile, 'a') as LogFile:
        for line in pListText:
            LogFile.write('\n'+ now + ' - ' + line)
        LogFile.close
    vDictEmail = {'Header':'Erro no SMART: ' + se.CNM_LISTENER, 'Subject': 'Status da Carga', 'To': pEmail }
    vDictEmail.update(se.vDictEmailLogin)
    vDictEmail['Status da carga'] = 'Um erro  foi encontrado na execucao do SMART, verifique a log para mais detalhes!!'
    email.send_mail_log(vDictEmail, pListText, se.CPATH_IMAGES)

def backup(pNameFile, pDirecFor, pDay):

    #Check the date is 1 and arquive exist in directory:
    if(datetime.datetime.now().day == pDay and os.path.isfile(pNameFile)):
        #Variavel name .zip:
        vFileNameZip = pNameFile +' - '+ str(datetime.datetime.now().day) + str("%02d" % (datetime.datetime.now().month,)) + str(datetime.datetime.now().year) +'.zip'
        #Create Zip:
        zf = zipfile.ZipFile(vFileNameZip,'w')
        zf.write(pNameFile)
        zf.close()  
        #Move arquive:
        shutil.move(vFileNameZip, pDirecFor)
        #Remove arquive pNameFile in directory
        os.remove(pNameFile)




