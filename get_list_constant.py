import database as db
from datetime import datetime
import settings as se
import database as db
import settings as se

def get_list_constant (con_ssba, pIdInterface, pNrLoadControl, vFileColumns,pDf):
    con = db.db_connection(se.vbDictDatabase['Name'], se.vbDictDatabase['String'])
    con_ssba = db.db_connection(se.vbDictDatabase['Name_ssba'], se.vbDictDatabase['String_ssba'])
    
    vDefault_value = db.execute_select(con, 'SELECT ds_default_value FROM public.tb_files_map WHERE id_interface = %s AND ds_default_value notnull ORDER BY id_column', [pIdInterface]).fetchall()

    #print(vDefault_value)
    
    vScript = 'SELECT MMD_SCHEMA.TB_MMD_COMPANY.CD_COMPANY, MMD_SCHEMA.TB_MMD_SYSTEM_SOURCES.CD_SYSTEM_SOURCE '\
        'FROM MMD_SCHEMA.TB_MMD_LIGHTHOUSE '\
        'INNER JOIN MMD_SCHEMA.TB_MMD_COMPANY ON ( MMD_SCHEMA.TB_MMD_COMPANY.ID_COMPANY = MMD_SCHEMA.TB_MMD_LIGHTHOUSE.ID_COMPANY )'\
        'INNER JOIN MMD_SCHEMA.TB_MMD_SYSTEM_SOURCES ON ( MMD_SCHEMA.TB_MMD_SYSTEM_SOURCES.ID_SYSTEM_SOURCE = MMD_SCHEMA.TB_MMD_LIGHTHOUSE.ID_SYSTEM_SOURCE )'\
        'WHERE MMD_SCHEMA.TB_MMD_LIGHTHOUSE.ID_INTERFACE = ' + str(pIdInterface)
    vListReturn = db.execute_select(con_ssba, vScript).fetchall()

    vCdCompany = list(vListReturn[0])[0]
    vCdSystemSource = list(vListReturn[0])[1]
    vCdCreatedBy = se.CNM_LISTENER
    '''
    vCdCompany = '2'
    vCdSystemSource = '1'
    vCdCreatedBy = 'teste'
    '''
    vListConstant = [vCdCompany, pNrLoadControl, vCdCreatedBy, vCdSystemSource, str(datetime.now())]
    vListNew = [('cd_company', ' varchar(20)'), ('nr_load_control', ' int4'), ('cd_created_by', ' varchar(20)'), ('cd_system_source', ' varchar(20)'), ('dt_processing', ' timestamp')]
    
    
    vFileColumnsCreate = [' '.join(x) for x in vFileColumns]
    for x in vListNew:
        vFileColumnsCreate.append(x)
    
    c=[]
    df=[]
    for b in vDefault_value:
        for i in b:
            c.append(i)
    for x in pDf:
        x+=c
        x+=vListConstant
        df.append(x)

    """ antigo insere constantes em um dataframe
    df['cd_company'] = pListConstant[0]"""

    return df, vFileColumnsCreate
